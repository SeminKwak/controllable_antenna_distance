clear all; close all; clc;
addpath('module');

bin = 1001;
u = linspace(-1,1,bin)';
N = 150;

A = arvu(N,u);
mu = N;
BW_s = 1;     % beam width for sumbeam (degree)
BW_d = 1.2;   % beam width for delbeam (degree)
u_r = sind(BW_d/2);
u_l = -u_r;
u_sr = sind(BW_s);
u_sl = -u_sr;
u_sr_d = sind(BW_d);
u_sl_d = -u_sr_d;
u_main = u(u<u_r&u>u_l);
u_bore = u(u==0);
u_side = u(u<u_sl|u>u_sr);
u_side_d = u(u<u_sl_d|u>u_sr_d);
u_side_cd = u(u<u_sl_d|u>u_sr_d);
A_main = arvu(N,u_main);
A_bore = arvu(N,u_bore);
dA_bore = derivAV(N,u_bore);
A_side = arvu(N,u_side);
A_side_d = arvu(N,u_side_d);
A_side_cd = arvu(N,u_side_cd);

rippledB = 0.4455;
l = sqrt(10^(-rippledB/10)*mu^2);
taudB_s = -21;
tau_s = sqrt(10^(taudB_s/10)*mu^2);

taudB_d = -21;
tau_d = sqrt(10^(taudB_d/10)*mu^2);
delta = 10^-5;

P = blkdiag(eye(N/2),-eye(N/2));
s = -100;

WS = 1;
WD = 1;
ws_b = ones(N,1);
wd = 1/2*(randn(N,1)+1j*randn(N,1));
wd_b = wd;
cost = inf;
inner = [];
ii = 1;

% DR algorithm
while cost(end) >= 10^-5
    phi_s = abs(wd);
    PHI_s = diag(phi_s);
    cvx_begin quiet
    variable ws(N) complex
    minimize (norm(PHI_s*ws,1))
    subject to
    A_bore*ws == mu;
    abs(A_side*ws)<=tau_s;
    cvx_end
    
    phi_d = abs(ws);
    PHI_d = diag(phi_d);
    
    cvx_begin quiet
    variable wd(N) complex
    minimize (norm(PHI_d*wd,1))
    subject to
    abs(A_side_cd*wd)<=tau_d;
    dA_bore*wd == 1j*s*mu;
    A_bore*wd == 0;
    
    cvx_end
    
    phi_s = abs(wd);
    PHI_s = diag(phi_s);
    
    factor = max( max(abs(ws)), max(abs(wd))) ;
    WS = ws/factor;
    WD = wd/factor;
    WS = abs(WS);
    WS(WS<=10^-3) = 0;
    WD = abs(WD);
    WD(WD<= 10^-3) =0;
    cost(ii) = (abs(ws_b)'*abs(wd_b)) - abs(ws)'*abs(wd);
    ws_b = ws;
    wd_b = wd;
    ii = ii +1;
end

% common weight
cvx_begin quiet
variable w(N)
variable t
minimize (t)
subject to
A_bore*w==mu;
A_bore*P*w == 0;
real(A_side*w) <=t;
real(A_side*w) >= -t;
imag(A_side_cd*P*w) <= t;
imag(A_side_cd*P*w) >= -t;
imag(dA_bore*P*w) == s*mu;
(w(1:(N)/2))== (w(N:-1:(N)/2+1));
imag(w) == 0;

cvx_end



theta = asind(u);


%% Figure
close all
cfig = figure(1);
plot(theta,20*log10(abs(A*w)/max(abs(A*w))),'k','LineWidth',1.5)
hold on
plot(theta,20*log10(abs(A*P*w)/max(abs(A*w))),'k-.','LineWidth',1.5, 'Color', [1 1 1]*0.5)
axis([-15 15 -30 0])
grid on
xlabel('\theta [deg]','FontName','Times New Roman')
ylabel({'Nomalized','array gain [dB]'},'FontName','Times New Roman')
hold off
legend({'F_\Sigma({\bf{w}}_C)', 'F_\Delta({\bf{w}}_C)'}, 'FontSize', 7);


set(cfig, 'Position', [500 500 300 180])

pfig = figure(2);
plot(theta,20*log10(abs(A*ws)/max(abs(A*ws))),'k','LineWidth',1.5)
hold on
plot(theta,20*log10(abs(A*wd)/max(abs(A*ws))),'k-.','LineWidth',1.5, 'Color', [1 1 1]*0.5)
axis([-15 15 -30 0])
grid on
xlabel('\theta [deg]','FontName','Times New Roman')
ylabel({'Nomalized','array gain [dB]'},'FontName','Times New Roman')
hold off
legend({'F_\Sigma({\bf{w}}_\Sigma )', 'F_\Delta({\bf{w}}_\Delta)'}, 'FontSize', 7)

set(pfig, 'Position', [500 500 300 180])

factor = max( max(abs(wd)), max(abs(ws)));
ws = ws/factor;
wd = wd/factor;
Ns = sum(abs(ws)>10^-2);
Nd = sum(abs(wd)>10^-2);

afig = figure(3);
W1 = abs(ws);
W1(W1 <= 10^-2) = NaN;
stem(linspace(-35,35,N),W1,'bp-','fill')
markerH = findall(gca,'color','b');
set(markerH,'MarkerFaceColor',[1 1 1]*0.1,'MarkerEdgeColor',[1 1 1]*0.1, 'Color', [1 1 1]*0.1, 'MarkerSize', 4);
hold on
W2 = abs(wd);
W2(W2 <= 10^-2) = NaN;
stem(linspace(-35,35,N),W2,'ro-','fill')
markerH = findall(gca,'color','r');
set(markerH,'MarkerFaceColor',[1 1 1]*1,'MarkerEdgeColor',[1 1 1]*0.3, 'Color', [1 1 1]*0.3, 'MarkerSize', 4);
h =legend('$\left| {\bf{w}}_\Sigma \right|$','$\left|{\bf{w}}_\Delta\right|$','Location','northeast','Orientation','horizontal');
set(h, 'Interpreter', 'latex')
xlabel('Antenna location [\lambda]','FontName','Times New Roman')
ylabel({'Nomalized';'magnitude'},'FontName','Times New Roman')
hold off
xlim([-35 35])
ylim([0 1.2])
set(afig, 'Position', [500 500 600 150]);
ax = gca;
ax.XTick = [-35:5:35];

afig = figure(4);
factor_w = max(abs(w));
w = w/factor_w;
W = abs(w);
stem(linspace(-35,35,N),W,'k.-','fill')
g =legend('$\left| {\bf{w}}_C \right|$','Location','northeast','Orientation','horizontal');
set(g, 'Interpreter', 'latex')
xlabel('Antenna location [\lambda]','FontName','Times New Roman')
ylabel({'Nomalized';'magnitude'},'FontName','Times New Roman')
xlim([-35 35])
ylim([0 1.2])
set(afig, 'Position', [500 500 600 150])
ax = gca;
ax.XTick = [-35:5:35];
