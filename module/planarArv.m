function A = planarArv(N,theta,phi)
M = N/2;
X = ones(N,1)*(-(M-1/2):1:M-1/2)*pi;
Y = pi*(-(M-1/2):1:M-1/2)'*ones(1,N);
%x = [X;Y];                                  % location vector
unit_DOA = -[cosd(phi).*sind(theta);sind(phi)];% ;cosd(phi)*cosd(theta)];    z elements are zero
dphi = unit_DOA(1)*X+ unit_DOA(2)*Y;
A = exp(1j*dphi);