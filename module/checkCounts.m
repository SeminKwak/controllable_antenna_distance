function [N, Nt, No] = checkCounts(W,thr)
W_ = zeros(size(W));
W_(abs(W)>thr)=1;
N = sum(W_,1);
No = sum(sum(W_,2)>=2);
Nt = sum(sum(W_));