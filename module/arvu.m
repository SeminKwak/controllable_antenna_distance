function a = arvu(N,u)
% ARVU   Make an array response vector with respect to u domain.
% a = arvu(N,u) makes an array response vector with number of
% antennas N and an angle u.
% ************************  INPUT *****************************
% *  u : an angle in u domain                                 *
% *  N : number of antennas                                   *
% ************************* OUTPUT ****************************
% *  a : array response vector                                *
% *************************************************************
u = u(:);
L = length(u);
if(~mod(N,2))
    M = N/2;
    temp = (-(M-1/2):1:M-1/2)*pi;
else
    M = floor(N/2);
    temp = (-M:1:M)*pi;
end
L2 = length(temp);
temp = ones(L,1)*temp;
uu = u*ones(1,L2);
a = exp(1j*temp.*uu);

