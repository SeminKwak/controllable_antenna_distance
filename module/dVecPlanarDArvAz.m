function A = dVecPlanarDArvAz(N,theta,phi,el)
M = N/2;
X = ones(N,1)*(-(M-1/2):1:M-1/2)*pi;
phi_side = phi((phi>=el(2))&(phi<=el(1)));
imax = length(phi_side);
A = zeros(N^2,imax);
for i = 1 : imax
    a_temp = planarArv(N,theta,phi_side(i));
    a = -1j*cosd(phi_side(i))*cosd(theta)*X.*a_temp;
    A(:,i) = a(:);
end