function A = dVecPlanarArv(N,theta,phi,az_rb,az_lb,el_ub,el_lb)
switch nargin
    case 7
        theta_side = theta((theta>=az_rb)|(theta<=az_lb));
        theta_side_inner = theta((theta<az_rb)&(theta>az_lb));
        phi_side = phi((phi>=el_ub)|(phi<=el_lb));
        
        i_max = length(theta_side)*length(phi) + length(theta_side_inner) + length(phi_side);
        A = zeros(N^2,i_max);
        
        k = 1;
        for j = 1 : length(phi)
            for i = 1 : length(theta_side)
                A_temp = planarArv(N,theta_side(i),phi(j));
                A(:,k) = A_temp(:);
                k = k+1;
            end
        end
        
        for i = 1 : length(theta_side_inner)
            for j = 1 : length(phi_side)
                A_temp = planarArv(N,theta_side_inner(i),phi_side(j));
                A(:,k) = A_temp(:);
                k = k+1;
            end
        end
        
    case 3
        theta_side = theta;
        phi_side = phi;
        i_max = length(theta_side)*length(phi_side);
        A = zeros(N^2,i_max);
        k = 1;
        for j = 1 : length(phi_side)
            for i = 1 : length(theta_side)
                A_temp = planarArv(N,theta_side(i),phi_side(j));
                A(:,k) = A_temp(:);
                k = k+1;
            end
        end
    otherwise
        error('The number of parameters are not enough')
end




%
% imax = length(theta_side);
% jmax = length(phi_side);
% kmax = imax*jmax;
% A = zeros(N^2,kmax);
%
% for k = 0 : kmax-1
%     A_temp = planarArv(N,theta_side(mod(k,imax)+1),phi_side(mod(k,jmax)+1));
%     A(:,k+1) = A_temp(:);
% end

%

% k = 1;
% for j = 1 : length(phi_side)
%     for i = 1 : length(theta_side)
%         A_temp = planarArv(N,theta_side(i),phi_side(j));
%         A(:,k) = A_temp(:);
%         k = k+1;
%     end
% end

