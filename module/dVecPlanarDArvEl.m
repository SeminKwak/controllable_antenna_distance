function A = dVecPlanarDArvEl(N,theta,phi,az)
M=N/2;
X = ones(N,1)*(-(M-1/2):1:M-1/2)*pi;
Y = pi*(-(M-1/2):1:M-1/2)'*ones(1,N);
theta_side = theta((theta>=az(2))&(theta<=az(1)));
imax = length(theta_side);
A = zeros(N^2,imax);
for i = 1 : imax
    a_temp = planarArv(N,theta_side(i),phi);
    a = 1j*(sind(phi)*sind(theta_side(i))*X-Y*cosd(phi)).*a_temp;
    A(:,i) = a(:);
end