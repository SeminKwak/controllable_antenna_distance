function [C, N_total] = circleArray(N)
lambda = 1;
x = lambda/4+(N/2-1)*lambda/2;
y = lambda/4;
r = sqrt(x^2+y^2);
C = zeros(N/2);
C(1,:) = 1;
for i = 1:N/2-1
    y_hat = lambda/4+lambda/2*i;
    x_hat = sqrt(r^2-y_hat^2);
    N_row = floor((x_hat - lambda/4)/(lambda/2))+1;
    C(i+1,1:N_row) = 1;
end
C = [flipud(C);C];
C = [fliplr(C) C];
N_total = sum(sum(C));
