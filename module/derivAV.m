function au = derivAV(N,u)
if(~mod(N,2))
    M = N/2;
    temp = 1j*(-(M-1/2):1:M-1/2)*pi;
else
    M = floor(N/2);
    temp = 1j*(-M:1:M)*pi;
end
L = length(u);
L2 = length(temp);
temp = ones(L,1)*temp;
uu = u*ones(1,L2);
au = temp.*exp(temp.*uu);