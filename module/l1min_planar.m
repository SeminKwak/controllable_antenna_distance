function [w,stat] = l1min_planar(N,ss,ds,taudB,s,alpha,thr,MODE)
% l1min(N,us,tau,s,alpha,mu)     reweighted l1 minimization algorithm(make sparcity)
% 
% Notice!!
% this function needs 'arv' and 'derivAV' functions.
% 
% <input>
% N     : the number of antennas
% us    : sidelobe region, it must be 2x1 column vector which elements
%         represent left starting point and right starting point. for
%         example us = [-0.1;0.1];
% tau   : sidelobe level upper bound    (dB)
% s     : DSR slope                     (It is not needed in sum beam)
% alpha : initial weight                (initial value = ones(N,1))
% mu    : stopping criterian            (initial value = 10^-3)
% MODE  : select sum(0), azimuth del(1), elevation del mode(2)
% <output>
% w     : sparse weight vector
% stat  : CVX status, if 0 then infeasible, 1 -> solved

switch nargin
    case 8
    case 7
    case 6
        thr = 10^-3;
    case 5
        thr = 10^-3;
        alpha = ones(N^2,1);
    case 4
        thr = 10^-3;
        alpha = ones(N^2,1);
        s = 0;
    otherwise
        error('The number of parameters are not enough')
end
M = N/2;
bin = 10;
bin3 = 11;
bin2 = 35;
theta_s = [linspace(-90,-20,bin2) linspace(-20,ss(2),bin) linspace(ss(2),ss(1),bin3) linspace(ss(1),20,bin) linspace(20,90,bin2)]';
theta_d = [linspace(-90,-20,bin2) linspace(-20,ds(2),bin) linspace(ds(2),ds(1),bin3) linspace(ds(1),20,bin) linspace(20,90,bin2)]';
% theta = linspace(-20,20,bin)';s
phi_s = theta_s;
phi_d = theta_d;
% phi = linspace(-20,20,bin)';


A_side_s = dVecPlanarArv(N,theta_s,phi_s,ss(1),ss(2),ss(1),ss(2));
A_side_d_az = dVecPlanarArv(N,theta_d,phi_d,ds(1),ds(2),ds(1),ds(2));
A_side_d_el = dVecPlanarArv(N,theta_d,phi_d,ds(1),ds(2),ds(1),ds(2));
% A_side_d_el = A_side_d_az;
A_bore = planarArv(N,0,0);
dA_bore_az = dVecPlanarDArvAz(N,0,phi_d,[0;0]);
dA_bore_el = dVecPlanarDArvEl(N,theta_d,0,[0;0]);
a_bore = A_bore(:);
P2 = blkdiag(eye(N^2/2),-eye(N^2/2));
I = eye(N^2/M/2);
P1_temp = blkdiag(eye(M),-eye(M));
P1 = kron(I,P1_temp);
[C,~]  =  circleArray(N);
c = C(:);

delta = 10^-5;
w = ones(N^2,1);
i = 1;
% w_norm = 1;
mu = N^2;
tau = sqrt(10^(taudB/10)*mu^2);
disp('begin cvx')
cvx_solver mosek
% cvx_solver_settings('MSK_IPAR_NUM_THREADS',1)
if (MODE == 0)
        PHI = diag(alpha);
        cvx_begin
            variable w(N^2) complex
            minimize (norm(PHI*w,1))
            subject to
            a_bore'*w==mu;
            abs(A_side_s'*w)<=tau;
            w == c.*w;
        cvx_end
        
elseif (MODE==1)
    PHI = diag(alpha);
    cvx_begin
            variable w(N^2) complex
            minimize (norm(PHI*w,1))
            subject to
            dA_bore_az'*w == mu*s;
            %dA_bore_el'*P1*w == s;
            abs(A_side_d_az'*w)<=tau;
            %abs(A_side_d_el'*P1*w)<=tau;
            a_bore'*w == 0;
            %a_bore'*P1*w == 0;
            w == c.*w;
        cvx_end
elseif (MODE == 2)
PHI = diag(alpha);
    cvx_begin
            variable w(N^2) complex
            minimize (norm(PHI*w,1))
            subject to
            %dA_bore_az'*P2*w == s;
            dA_bore_el'*w == mu*s;
            %abs(A_side_d_az'*P2*w)<=tau;
            abs(A_side_d_el'*w)<=tau;
            a_bore'*w == 0;
            %a_bore'*P1*w == 0;
            w == c.*w;
        cvx_end
  end
if (strcmp(cvx_status,'Infeasible'))
    stat = 0;
else
    stat = 1;
%     w = w/max(abs(w));
end
