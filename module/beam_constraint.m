function [c,ceq] = beam_constraint(x,A_side,A_side_d,tau_s,tau_d)
x = x(:);
M = length(x);
N = M/2;
c = [abs(A_side*x(1:N))-tau_s;abs(A_side_d*x(N+1:end))-tau_d];
ceq = [];

