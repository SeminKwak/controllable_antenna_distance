function J = costfunc(x)
x = x(:);
L = length(x);
J = abs(x(1:L/2))'*abs(x(L/2+1:end));