clc; clear all; close all;
addpath('module');

N = 32;
M = N/2;
slope = -22;
ss = [5;-5];
ds = [8;-8];
taudB = -25;

mu = 10^-3;
alpha = ones(N^2,1);
tic
k = 0;

Ns = zeros(3,1);
Nd = zeros(3,1);
No = [];
ws = ones(N^2,1);
wda = ones(N^2,1);
wde = ones(N^2,1);
inner = abs(ws)'*abs(wda)+abs(wda)'*abs(wde)+abs(wde)'*abs(ws);
cost = N^2;
thereshold = 10^-5;
i = 1;
while cost(end) >= thereshold;
    [ws,~] = l1min_planar(N,ss,ds,taudB,0,alpha,1,0);
    alpha = abs(ws) + abs(wde);
    
    [wda,~] = l1min_planar(N,ss,ds,taudB,slope,alpha,1,1);
    alpha = abs(wda) + abs(ws);
    
    [wde,~] = l1min_planar(N,ss,ds,taudB,slope,alpha,1,2);
    alpha = abs(wde) + abs(wda);
    
    [~,~,No(i)] = checkCounts([ws wde wda], 10^-3);
    inner = [inner;abs(ws)'*abs(wda)+abs(wda)'*abs(wde)+abs(wde)'*abs(ws)];
    cost = [cost;inner(end-1)-inner(end)];
    i = i + 1;
end
toc/60/60

bin = 301;
theta = linspace(-90,90,bin)';
phi = linspace(-90,90,bin)';

P2 = blkdiag(eye(N^2/2),-eye(N^2/2));
I = eye(N^2/M/2);
P1_temp = blkdiag(eye(M),-eye(M));
P1 = kron(I,P1_temp);
A = dVecPlanarArv(N,theta,phi);

sumbeam = A'*ws;
sumbeam = reshape(sumbeam,length(phi),length(theta));
del_az = A'*wda;
del_az = reshape(del_az,length(phi),length(theta));
del_el = A'*wde;
del_el = reshape(del_el,length(phi),length(theta));

WS = reshape(ws,N,N);
WDA = reshape(wda,N,N);
WDE = reshape(wde,N,N);
[azi,el] = meshgrid(-90:theta(2)-theta(1):90);
n_sumbeam=20*log10(abs(sumbeam)/max(max(abs(sumbeam))));
n_del_az = 20*log10(abs(del_az)/max(max(abs(sumbeam))));
n_del_el = 20*log10(abs(del_el)/max(max(abs(sumbeam))));

%% figure
close all
figure,
subplot(121)
plot(1:i-1, (inner(2:end)),'ko-','LineWidth',2);
xlabel('Iteration number');
ylabel(['The cost J']);
ax = gca;
ax.XTick = [1:i-1];
xlim([1 i-1])
ylim([0 max(inner(2:end))])
grid on

subplot(122)
plot((No),'ko-','LineWidth',2);
xlabel('Iteration number');
ylabel({'The number of';'overlapped antennas'});
ylim([0 max(No)]);
ax = gca;
ax.XTick = [1:length(No)];
xlim([1 length(No)])
grid on




figure('Position', [300, 300, 500, 150])
surf(azi,el,n_sumbeam');
axis([-20 20 -20 20 taudB-10 0 taudB-40 0])
xlabel('\theta [deg]','FontName','Times New Roman')
ylabel('\phi [deg]','FontName','Times New Roman')
zlabel({'Nomalized';'array gain [dB]'},'FontSize',10,'FontName','Times New Roman')
colormap('gray')
view(45, 20);
set(gcf, 'renderer','painters')
figure('Position', [300, 300, 500, 150])
surf(azi,el,n_del_az');
axis([-20 20 -20 20 taudB-10 0 taudB-40 0])
caxis([-60 0])
xlabel('\theta [deg]','FontName','Times New Roman')
ylabel('\phi [deg]','FontName','Times New Roman')
zlabel({'Nomalized';'array gain [dB]'},'FontSize',10,'FontName','Times New Roman')
colormap('gray')
view(45, 20);
set(gcf, 'renderer','painters')
figure('Position', [100, 100, 500, 150])
surf(azi,el,n_del_el');
axis([-20 20 -20 20 taudB-10 0 taudB-40 0])
caxis([-60 0])
xlabel('\theta [deg]','FontName','Times New Roman')
ylabel('\phi [deg]','FontName','Times New Roman')
zlabel({'Nomalized';'array gain [dB]'},'FontSize',10,'FontName','Times New Roman')
colormap('gray')
view(45, 20);
set(gcf, 'renderer','painters')

T = ones(size(WS));
S = abs(WS)>10^-3;
DA = abs(WDA)>10^-3;
DE = abs(WDE)>10^-3;
Ns = sum(sum(S));
Nda = sum(sum(DA));
Nde = sum(sum(DE));
[C,~]  =  circleArray(N);
figure('Position',[100 100 500 500]),
hold on
spy(S,'bp',7);
markerH = findall(gca,'color','b');
set(markerH,'MarkerFaceColor',[1 1 1]*0.1,'MarkerEdgeColor',[1 1 1]*0.1);

spy(DA,'ro',7);
markerH = findall(gca,'color','r');
set(markerH,'MarkerFaceColor',[1 1 1]*1,'MarkerEdgeColor',[1 1 1]*0.3);
spy(DE,'go',7);
markerH = findall(gca,'color','g');
set(markerH,'MarkerFaceColor',[1 1 1]*0.5,'MarkerEdgeColor',[1 1 1]*0.6);

style = hgexport('factorystyle');
style.Bounds = 'Tight';
hgexport(gcf, '-Clipboard', style, 'ApplyStyle', true);
hold off
legend({'\Sigma','\Delta_{Az}','\Delta_{El}'},'FontSize',12,'Location','northwest','Orientation','vertical')
axis off

%save('Fig4Variables.mat')