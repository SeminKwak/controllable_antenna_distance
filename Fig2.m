close all; clc; clear all;
addpath('module');
%% parameter setting
bin = 131;
u = linspace(-1,1,bin)';
N = 50;
mu = N;     % directivity factor
BW = 6;     % beam width

% array response matrices
A = arvu(N,u);
u_sr = sind(BW/2);
u_sl = -u_sr;
u_bore = u(u==0);
u_side = u(u<u_sl|u>u_sr);

A_bore = arvu(N,u_bore);
A_side = arvu(N,u_side);

% peak sidelobe level
taudB_s = -22;
tau_s = sqrt(10^(taudB_s/10)*mu^2);
delta = 10^-5;


thr = 10^-3;

% initial conditions
alpha = 1;
w = rand(N,1);
c_b = 100;
%wd_b = ones(N-1,1);
cost = inf;
inner = [];
No = [];
Nt = [];
i = 1;
%while cost(end) >= thr
for i=1:10
    if (floor(i/2)==i/2)
        p = abs(w);
        p = [0;p];
        p = p(1:N);
        P = diag(p);
    else
        p = abs(w);
        p = [p;0];
        p = p(2:N+1);
        P = diag(p);
    end
    cvx_begin quiet
    variable w(N) complex
    minimize (norm(P*w,1))
    subject to
    A_bore*w == mu;
    abs(A_side*w)<=tau_s;
    cvx_end
    
    cost(i) = (c_b) - norm(P*w,1);
    inner(i) = norm(P*w,1);
    c_b = norm(P*w,1);
    
    % figure
    afig = figure(i);
    W1 = abs(w);
    W1(W1 <= thr) = NaN;
    stem(1:N,W1,'bp-','fill')
    markerH = findall(gca,'color','b');
    set(markerH,'MarkerFaceColor',[1 1 1]*0.1,'MarkerEdgeColor',[1 1 1]*0.1, 'Color', [1 1 1]*0.1);
    %set(h, 'Interpreter', 'latex')
    xlabel('Antenna element number','FontName','Times New Roman')
    ylabel({'Nomalized';'magnitude'},'FontName','Times New Roman')
    xlim([0 N+1])
    ylim([0 2])
    set(afig, 'Position', [100 500 600 150])
    drawnow;
    %i = i + 1;
end

%% Figures of beam pattern and phase

% figure of the cost function J
figure,
subplot(121)
plot(1:i-1, inner,'ko-','LineWidth',2);
xlabel('Iteration number');
ylabel('The cost J');
ax = gca;
ax.XTick = [1:i-1];
ax.YTick = [0:max(inner)];
xlim([1 i-1])
grid on

% figure of the overlapped antennas
subplot(122)
plot(No,'ko-','LineWidth',2);
xlabel('Iteration number');
ylabel('The number of overlapped antennas');
ylim([0 max(No)]);
ax = gca;
ax.XTick = [1:length(No)];
ax.YTick = [0:max(No)];
xlim([1 length(No)])
grid on

%% save all variables
%save('Fig2Variables.mat')

